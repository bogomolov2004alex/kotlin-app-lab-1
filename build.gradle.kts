plugins {
    kotlin("jvm") version "1.9.22"
    id("com.adarshr.test-logger") version "4.0.0"
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://plugins.gradle.org/m2/")
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.9.22")
    testImplementation("com.adarshr:gradle-test-logger-plugin:4.0.0")
    testImplementation(kotlin("test"))
}

tasks.jar {
    manifest {
        attributes(
            "Main-Class" to "ru.mirea.MainKt"
        )
    }
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
}

tasks.test {
    useJUnitPlatform()
}
