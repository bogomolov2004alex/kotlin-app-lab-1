FROM gradle:jdk17 as builder
WORKDIR /app
ADD . .
RUN pwd
RUN ls .
RUN ./gradlew clean jar
RUN ls /app/build/libs/

FROM openjdk:17 as backend
WORKDIR /root
COPY --from=builder /app/build/libs ./
ENTRYPOINT ["java", "-jar", "kotlin-search-app.jar"]