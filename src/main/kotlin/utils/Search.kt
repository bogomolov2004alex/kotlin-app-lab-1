package utils

class Search {
    companion object {
        fun binary(array: IntArray, target: Int): Int {
            var left = 0
            var right = array.size - 1

            while (left <= right) {
                val mid = left + (right - left) / 2

                when {
                    array[mid] == target -> return mid
                    array[mid] < target -> left = mid + 1
                    else -> right = mid - 1
                }
            }

            return -1
        }
    }
}